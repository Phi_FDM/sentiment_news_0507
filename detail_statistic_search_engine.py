import scrapy
import bs4
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import re
import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import date
import datetime
import pymysql
import collections
import pandas as pd
import matplotlib.pyplot as plt

#--------------
def connect():
    conn = pymysql.connect(host='192.168.41.19',
                           port=13306,
                           user='sentiment',
                           password='123456',
                           db='sentiment',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    return conn

with connect().cursor() as cursor:
    query = "Show columns FROM sentiment.mxh_news_by_industry;"
    con = connect().cursor()
    con.execute(query)
    columns = con.fetchall()

with connect().cursor() as cursor:
    query = "SELECT * FROM sentiment.mxh_news_by_industry;"
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()

def statistic_by_stock(stock):
    #stock='ACB'
    df=pd.DataFrame([[a['id'],a['website_name'],a['titles'],a['discription'],a['content_text_only'],a['stock'],a['original_links'],a['publishdate'],a['downloaded_at'],a['sentiment_tag']] for a in results],columns=['id','website_name','titles','discription','content_text_only','stock','original_links','publishdate','downloaded_at','sentiment_tag'])
    df=df[df.stock==stock][df.website_name!='https://s.cafef.vn'][df.website_name!='https://vietstock.vn'].reset_index()
    date_list=[]
    time_list=[]
    weekday_list=[]
    for i in list(df['publishdate']):
        date_list.append(date(i.year,i.month,i.day))
        time_list.append(datetime.time(i.hour))#,i.minute))
        weekday_list.append(i.weekday())
    df['date']=date_list
    df['time']=time_list
    df['weekday']=weekday_list
    return df

def connect():
    conn = pymysql.connect(host='192.168.41.19',
                           port=13306,
                           user='sentiment',
                           password='123456',
                           db='sentiment',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    return conn
with connect().cursor() as cursor:
    query = "SELECT name FROM sentiment.mxh_industry_with_user;"
    con = connect().cursor()
    con.execute(query)
    industry_list = con.fetchall()

j=19   #index of list of industry
#--------------
with connect().cursor() as cursor:
    query = "SELECT stock FROM sentiment.mxh_stocks where industry='"+industry_list[j]['name']+"' and market='HSX';"
    con = connect().cursor()
    con.execute(query)
    results2 = con.fetchall()
stock_list=[a['stock'] for a in results2]
print(industry_list[j]['name'])
print(len(stock_list))

acb=statistic_by_stock(stock_list[0])#pd.DataFrame()#columns=['url_link', 'title', 'published_date_on_article', 'description','content', 'website_name', 'stock_new', 'downloaded_at', 'status'])
for stock in stock_list[1:]:
    acb=acb.append(statistic_by_stock(stock),ignore_index=True)

#------------------------------
acb1=acb['date']
counter=collections.Counter(list(acb1))
e=[]
for i in list(counter):
    e.append({'DATETIME':i,'Population':counter[i]})
df1 = pd.DataFrame(e)
df1.index = df1['DATETIME']
df1.index = (pd.to_datetime(df1.index)).strftime("%m/%d")
df1.Population.plot.bar()
plt.tick_params(axis='both', which='both', labelsize=7)
plt.xticks(plt.xticks()[0][1::2],
           plt.xticks()[1][1::2])
plt.tight_layout()
plt.show()
#------------------------------
acb2=acb['weekday']
counter2=collections.Counter(list(acb2))
e2=[]
for i in list(counter2):
    e2.append({'WEEKDAY':i,'Population':counter2[i]})
df2 = pd.DataFrame(e2)
counter2
df2.index = df2['WEEKDAY']
df2.Population.plot.bar()
plt.tick_params(axis='both', which='both', labelsize=7)

plt.xticks(plt.xticks()[0][1::2],
           plt.xticks()[1][1::2])

plt.tight_layout()
plt.show()
#------------------------------
acb3=acb['time']
counter3=collections.Counter(list(acb3))
e3=[]
for i in list(counter3):
    e3.append({'HOUR':i,'Population':counter3[i]})
df3 = pd.DataFrame(e3)
counter2
df3.index = df3['HOUR']
df3.Population.plot.bar()
plt.tick_params(axis='both', which='both', labelsize=7)

plt.xticks(plt.xticks()[0][1::2],
           plt.xticks()[1][1::2])

plt.tight_layout()
plt.show()
#------------------------------
df_tag=pd.DataFrame([[a['id'],a['website_name'],a['stock'],a['publishdate'],a['downloaded_at'],a['sentiment_tag']] for a in results],columns=['id','website_name','stock','publishdate','downloaded_at','sentiment_tag'])
a = datetime.date(2021, 3, 22)


def statistic_by_day(date1):
    df_tag1 = pd.DataFrame(
        [[a['id'], a['website_name'], a['stock'], a['publishdate'], a['sentiment_tag']] for a in results],
        columns=['id', 'website_name', 'stock', 'publishdate', 'sentiment_tag'])
    df_tag1 = df_tag1[df_tag1.website_name != 'https://s.cafef.vn'][
        df_tag1.website_name != 'https://vietstock.vn'].reset_index()
    date_list = []
    for i in list(df_tag1['publishdate']):
        try:
            date_list.append(date(i.year, i.month, i.day))
        except TypeError:
            date_list.append('')
    df_tag1['date'] = date_list
    df_tag1 = df_tag1[df_tag1.date == date1]

    return df_tag1

print(statistic_by_day(a))
b=statistic_by_day(a)
import collections
p = list(b['stock'])
counter4=collections.Counter(p)
print(counter4.most_common(4))