import scrapy
import bs4
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import re
import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import date
import pymysql
import datetime

def connect():
    conn = pymysql.connect(host='192.168.41.19',
                           port=13306,
                           user='sentiment',
                           password='123456',
                           db='sentiment',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    return conn

industry_index=range(29,37) #index of list of industry 19 bank,
stock_list=[]
for j in industry_index:
    with connect().cursor() as cursor:
        query = "SELECT name FROM sentiment.mxh_industry_with_user;"
        con = connect().cursor()
        con.execute(query)
        results = con.fetchall()
#--------------
    with connect().cursor() as cursor:
        query = "SELECT stock FROM sentiment.mxh_stocks where industry='"+results[j]['name']+"';"# and market='HSX';"
        con = connect().cursor()
        con.execute(query)
        results2 = con.fetchall() 
        stock_list=stock_list+[a['stock'] for a in results2]
        print(results[j]['name'])
        print(len(results2))
print('len total stock_list:',len(stock_list))
stock_list


def get_news_tnck(stock):
    url = "https://tinnhanhchungkhoan.vn/search/" + stock + ".html"
    soup = BeautifulSoup(requests.get(url).text)
    html = urlopen(url)
    bsObj = BeautifulSoup(html.read())
    href_link = []
    for j in list(bsObj.find_all("figure", {"class": "story__thumb"})):
        href_link.append(j.a['href'])
    bsObj1 = str(bsObj)
    for link in href_link:
        if bsObj1.index(link) < bsObj1.index('box-content'):
            pass
        else:
            href_link.remove(link)
    href_link = href_link[:len(href_link) - 3]
    url_link = []
    datetime = []
    title = []
    description = []
    content = []
    image = []
    for link in href_link:
        try:
            html_article = urlopen(link)
        except ValueError:
            pass

        bsObj = BeautifulSoup(html_article.read())
        try:
            title.append(bsObj.find('h1', {'class': 'article__header cms-title'}).getText().strip())
            datetime.append(bsObj.find("div", {"class": "article__meta"}).find('time').getText().strip())
            description.append(
                bsObj.find('div', {'class': 'article__sapo cms-desc'}).getText().strip().replace('(ĐTCK) ', ''))
            content.append(bsObj.find('div', {'class': 'article__body cms-body'}).getText().strip())
            image.append(bsObj.find('img', {'class': 'cms-photo'}))
            url_link.append(link)
        except AttributeError:
            pass
        image2 = []
        for j in image:
            try:
                image2.append(j['data-photo-original-src'])
            except TypeError:
                image2.append('')
        publishdate = []
        for a in datetime:
            a = a.split()
            if '/' in a[0] and ':' in a[1]:
                publishdate.append(a[0][6:] + '-' + a[0][3:5] + '-' + a[0][:2] + ' ' + a[1])
            else:
                publishdate.append('')

    df = pd.DataFrame()

    df['url_link'] = url_link
    df['title'] = title
    df['publishdate'] = publishdate
    df['description'] = description
    df['content'] = content
    df['image'] = image2

    df['website_name'] = ['https://tinnhanhchungkhoan.vn/'] * len(df)
    df['stock_new'] = [stock] * len(df)
    df['downloaded_at'] = [str((date.today()))] * len(df)
    df['status'] = [0] * len(df)

    return df

#----------
crawl_df=get_news_tnck(stock_list[0])#pd.DataFrame()#columns=['url_link', 'title', 'published_date_on_article', 'description','content', 'website_name', 'stock_new', 'downloaded_at', 'status'])
for stock in stock_list:#[1:3]:
    crawl_df=crawl_df.append(get_news_tnck(stock),ignore_index=True)
#----------
print(len(crawl_df))
print('number of stock:',len(set(list(crawl_df['stock_new']))))

with connect().cursor() as cursor:
    query = 'SELECT * FROM sentiment.mxh_news_by_industry'
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()
last_id=results[len(results)-1]['id']
#results[0].keys()
print('last_id:',last_id)
with connect().cursor() as cursor:
    query = "SELECT original_links FROM mxh_news_by_industry where website_name = 'https://tinnhanhchungkhoan.vn/'"
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()
i1=[]
for i in range(0,len(crawl_df)):
    if crawl_df.iloc[i]['url_link'] in [a['original_links'] for a in results]:
        i1.append(i)
    else:
        pass
crawl_df=crawl_df.drop(i1)#.drop(columns=['index'])


crawl_df=crawl_df[crawl_df.publishdate!=''].reset_index()
crawl_df=crawl_df.drop(columns=['index'])

print(len(crawl_df))

#query='insert into sentiment.mxh_news_by_industry (id,original_links,titles,publishdate,discription,content_text_only,original_images,website_name, stock,downloaded_at,status) values ;'
query2=tuple([tuple([last_id+i+1]+list(crawl_df.iloc[i])) for i in range(0,len(crawl_df))])
print(query2)
