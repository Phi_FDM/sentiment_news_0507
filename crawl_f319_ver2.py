import scrapy
import bs4
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import re
import pandas as pd
import requests
from datetime import date
import pymysql
import datetime
import json

divided_range_of_thread_to_crawl=range(0,10)
#---------------
with open('f319_thread_link_198980.json') as f:
    title_thread_dict = json.load(f)

divided_link_of_thread_to_crawl=[title_thread_dict[i]['url_link'] for i in divided_range_of_thread_to_crawl]
divided_title_of_thread_to_crawl=[title_thread_dict[i]['title'] for i in divided_range_of_thread_to_crawl]


#---------------
total_num_pages_list=[]
all_text_list=[]
for thread in divided_link_of_thread_to_crawl:
    z=urlopen(thread)
    content=BeautifulSoup(z.read())
    try:
        total_num_pages=int(content.find_all("span", attrs={"class" : "pageNavHeader"})[0].getText().strip().split('/')[1])
    except IndexError:
        total_num_pages=1
    all_text=[]
    for j in range (1,total_num_pages+1):
        page_in_thread = thread+"page-"+str(j)
        z=urlopen(page_in_thread)
        content=BeautifulSoup(z.read())
        each_text=[]
        for tag in content.find_all('article'):
            try:
                a=tag.blockquote.aside.decompose()
                each_text.append(re.sub('\s+', ' ',a.text).strip())
            except AttributeError:
                each_text.append(re.sub('\s+', ' ',tag.text).strip())
        all_text=all_text+each_text
    total_num_pages_list.append(total_num_pages)
    all_text_list.append(all_text)
print('total_num_pages_list: ',total_num_pages_list)
print('total comment in each thread: ',[len(i) for i in all_text_list])
#---------------
all_mentioned_stock=[]
edited_title=[]
characters_to_remove = "_:.,-!()@"
for title in divided_title_of_thread_to_crawl:
    new_string = title
    for character in characters_to_remove:
          new_string = new_string.replace(character, " ")
    edited_title.append(new_string)

for title in edited_title:
    all_mentioned_stock.append([stock for stock in stock_list if stock in title.upper().split()])

#---------------
total_dict=[{'url_link':divided_link_of_thread_to_crawl[i],
  'title':divided_title_of_thread_to_crawl[i],
  'mentioned_stock':all_mentioned_stock[i],
  'comment':all_text_list[i]} for i in range(0,len(divided_range_of_thread_to_crawl))]
#---------------
with open('content_thread.json', 'w') as f:
    json.dump(total_dict, f)

