import scrapy
import bs4
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import re
import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import date
import pymysql
import datetime

def connect():
    conn = pymysql.connect(host='192.168.41.19',
                           port=13306,
                           user='sentiment',
                           password='123456',
                           db='sentiment',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    return conn
industry_index=range(20,29) #index of list of industry 19 bank,
stock_list=[]
for j in industry_index:
    with connect().cursor() as cursor:
        query = "SELECT name FROM sentiment.mxh_industry_with_user;"
        con = connect().cursor()
        con.execute(query)
        results = con.fetchall()
#--------------
    with connect().cursor() as cursor:
        query = "SELECT stock FROM sentiment.mxh_stocks where industry='"+results[j]['name']+"';"# and market='HSX';"
        con = connect().cursor()
        con.execute(query)
        results2 = con.fetchall() 
        stock_list=stock_list+[a['stock'] for a in results2]
        print(results[j]['name'])
        print(len(results2))
print('len total stock_list:',len(stock_list))


def get_news_24hmoney(stock):
    
    url = "https://24hmoney.vn/stock/"+stock+"/news"
    soup = BeautifulSoup(requests.get(url).text)
    html = urlopen(url)
    bsObj = BeautifulSoup(html.read())
    href_link=[]
    for j in list(bsObj.find_all("div",{"class":"ctn"})):
        
        href_link.append(j.header.a.attrs["href"])
    url_link=[]
    datetime=[]
    title=[]
    description=[]
    content=[]
    image=[]
    for link in href_link:
        try:
            html_article = urlopen('https://24hmoney.vn'+link)
        except ValueError:
            pass
        
        bsObj = BeautifulSoup(html_article.read())
        try: 
            title.append(bsObj.find('h1',{'class':'news-title'}).getText().strip())
        except AttributeError: 
            title.append('')
        try:    
            datetime.append(bsObj.find('time',{'class':'published-date'}).getText().strip())
        except AttributeError: 
            datetime.append('')
        try:    
            description.append(bsObj.find('h2',{'class':'news-sapo'}).getText().strip())
        except AttributeError: 
            description.append('')
        try:    
            content.append(bsObj.find('div',{'class':'news-content'}).getText().strip())
        except AttributeError:  
            content.append('')
        try:    
            image.append(bsObj.find('div',{'class':'news-thumb first'}).img['src'])
        except AttributeError:
            image.append('')
            
        publishdate=[]
        for a in datetime:
            a=a.split()
            if a[3][6:].isdigit() and a[3][3:5].isdigit() and a[3][:2].isdigit() and a[4][:2].isdigit() and a[4][3:].isdigit():
                publishdate.append(a[3][6:]+'-'+a[3][3:5]+'-'+a[3][:2]+' '+a[4][:2]+':'+a[4][3:])
            else:
                publishdate.append('')

    df=pd.DataFrame()
    #forex_df['']=
    df['url_link']=url_link
    df['title']=title
    df['publishdate']=publishdate
    df['description']=description
    df['content']=content
    df['image']=image
    
    df['website_name']=['https://24hmoney.vn']*len(df)
    df['stock_new']=[stock]*len(df)
    df['downloaded_at']=[str((date.today()))]*len(df)
    df['status']=[0]*len(df)

    return df

#----------
crawl_df=pd.DataFrame()
for i in range(0,len(stock_list)-1):
    try:
        data=get_news_24hmoney(stock_list[i])
        
    except UnboundLocalError:
        data=pd.DataFrame()#stock_list.remove(stock_list[i])
        print('UnboundlocalError stock:',stock_list[i])
        pass
    crawl_df=crawl_df.append(data,ignore_index=True)   
print('new len of stock list after removing UnboundError:',len(stock_list))
print('len(crawl_df):',len(crawl_df))
print('number of stock:',len(set(list(crawl_df['stock_new']))))

with connect().cursor() as cursor:
    query = 'SELECT * FROM sentiment.mxh_news_by_industry'
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()
last_id=results[len(results)-1]['id']
#results[0].keys()
print('last_id given in DB:',last_id)
with connect().cursor() as cursor:
    query = "SELECT original_links FROM mxh_news_by_industry where website_name = 'https://24hmoney.vn'"
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()
i1=[]
for i in range(0,len(crawl_df)):
    if crawl_df.iloc[i]['url_link'] in [a['original_links'] for a in results]:
        i1.append(i)
    else:
        pass
crawl_df=crawl_df.drop(i1)#.drop(columns=['index'])
print('length of data after removing duplicated:',len(crawl_df))
#-------------------
#query='insert into sentiment.mxh_news_by_industry (id,original_links,titles,publishdate,discription,content_text_only,original_images,website_name, stock,downloaded_at,status) values ;'
query2=tuple([tuple([last_id+i+1]+list(crawl_df.iloc[i])) for i in range(0,len(crawl_df))])
print(query2)
