import mysql.connector
import pandas as pd

def _term_dict():
    cnx = mysql.connector.connect(
    host="192.168.41.19",
    port=13306,
    user="sentiment",
    password="123456")
    # Get a cursor
    cur = cnx.cursor()

    data=[]
    cur.execute("select * from sentiment.sentiment_termdict")
    for row in cur:
        data.append(row)
    return pd.DataFrame(data,columns=['term','label'])
_term_dict()

#cur.execute("INSERT INTO crawler.term_dict (term,label,editor_name) values ('phần chìm,'positive','Phi')")