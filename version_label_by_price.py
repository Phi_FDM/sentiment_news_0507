import pandas as pd
import matplotlib.pyplot as plt
import datetime


def get_percentage_change(stock, lag):
    # timeframe = [20183112,20200101]
    daily_price = pd.read_csv('./price_history_demo_1706/historical-price-{}0201201717062020.csv'.format(stock))
    # daily_price = daily_price[daily_price.DTYYYYMMDD > timeframe[0]][daily_price.DTYYYYMMDD < timeframe[1]]
    daily_price = daily_price.sort_values(by=['DATE']).reset_index()

    # year=[]
    # month=[]
    # date=[]
    # for i in range(0,len(daily_price)):
    #    year.append(int(str(daily_price.iloc[i]['DATE'])[6:10]))
    #    month.append(int(str(daily_price.iloc[i]['DATE'])[3:5]))
    #    date.append(int(str(daily_price.iloc[i]['DATE'])[:2]))
    trading_day = []
    for i in range(0, len(daily_price)):
        trading_day.append(datetime.datetime.strptime(daily_price.iloc[i]['DATE'], "%d/%m/%Y"))
    daily_price['trading_day'] = trading_day
    daily_price = daily_price.sort_values(by=['trading_day']).reset_index()

    price_change = [0] * lag
    for i in range(0, len(daily_price) - lag):
        price_change.append(
            (daily_price.iloc[i + lag]['CLOSE'] - daily_price.iloc[i]['CLOSE']) / daily_price.iloc[i]['CLOSE'])
    price_change = [round(price_change[i], 5) for i in range(0, len(price_change))]

    daily_price['price_change'] = price_change
    price_trend = []
    for i in range(0, len(daily_price)):
        if daily_price.iloc[i]['price_change'] > 0:
            price_trend.append('up')
        if daily_price.iloc[i]['price_change'] < 0:
            price_trend.append('down')
        if daily_price.iloc[i]['price_change'] == 0:
            price_trend.append('no change')
    daily_price['price_trend'] = price_trend

    volume_change = [0] * lag
    for i in range(0, len(daily_price) - lag):
        volume_change.append(
            (daily_price.iloc[i + lag]['VOLUME'] - daily_price.iloc[i]['VOLUME']) / daily_price.iloc[i]['VOLUME'])
    volume_change = [round(volume_change[i], 5) for i in range(0, len(volume_change))]

    daily_price['volume_change'] = volume_change
    volume_trend = []
    for i in range(0, len(daily_price)):
        if daily_price.iloc[i]['volume_change'] > 0:
            volume_trend.append('up')
        if daily_price.iloc[i]['volume_change'] < 0:
            volume_trend.append('down')
        if daily_price.iloc[i]['volume_change'] == 0:
            volume_trend.append('no change')
    daily_price['volume_trend'] = volume_trend

    date = [daily_price.iloc[i]['trading_day'].date() for i in range(0, len(daily_price))]
    daily_price['date'] = date
    daily_price = daily_price.drop(['level_0', 'index', 'DATE', 'OPEN', 'HIGH', 'LOW'], axis=1)
    return daily_price


#get_percentage_change(stock='PNJ', lag=5)

investor_appetite = 0.05
lag=5# rate that investor tend to take risk and return

def get_abnormal_return(stock,lag,investor_appetite):
    q=get_percentage_change(stock,lag)
    q=pd.concat([q[q.price_change <investor_appetite*-1],q[q.price_change > investor_appetite]])
    return q
#get_abnormal_return('HPG',5,0.05)

def get_trading_signal_date(stock,lag,investor_appetite):
    lag=lag
    action = []
    b=get_abnormal_return(stock,lag,investor_appetite)
    c=get_percentage_change(stock,lag)
    list_index = list(b.index.values)
    for j in range(lag,len(c)):
        if j in list_index:
            if b.loc[j]['price_trend'] == 'up':
                action.append([c.loc[j-lag]['trading_day'],'BUY'])
            if b.loc[j]['price_trend'] == 'down':
                action.append([c.loc[j-lag]['trading_day'],'SELL'])
        #else:
        #    if j not in list_index:
        #        action.append('do nothing')
    #action.extend(['no relevant data to conclude']*lag)
    return action
get_trading_signal_date(stock = 'PNJ',lag = 5,investor_appetite = 0.05)


def get_buy_date(stock, lag, investor_appetite):
    action = []
    b = get_abnormal_return(stock, lag, investor_appetite)
    c = get_percentage_change(stock, lag)
    list_index = list(b.index.values)
    for j in range(lag, len(c)):
        if j in list_index:
            if b.loc[j]['price_trend'] == 'up':
                action.append([j - lag, c.loc[j - lag]['trading_day']])

    return action
# c=get_buy_date(stock = 'HPG',lag = 5,investor_appetite = 0.05)#[:0]

def get_sell_date(stock, lag, investor_appetite):
    action = []
    b = get_abnormal_return(stock, lag, investor_appetite)
    c = get_percentage_change(stock, lag)
    list_index = list(b.index.values)
    for j in range(lag, len(c)):
        if j in list_index:
            if b.loc[j]['price_trend'] == 'down':
                action.append([j - lag, c.loc[j - lag]['trading_day']])

    return action
# get_sell_date(stock = 'HPG',lag = 5,investor_appetite = 0.05)

def visualize_price(stock, lag, investor_appetite):
    daily_price = get_percentage_change(stock, lag)

    no_action_day = daily_price.drop(list(get_abnormal_return(stock, lag, investor_appetite).index))[
        ['trading_day', 'CLOSE']]  # .set_index('trading_day')

    c1 = get_buy_date(stock, lag, investor_appetite)
    buy_day = daily_price.loc[[c1[i][0] for i in range(0, len(c1))]][
        ['trading_day', 'CLOSE']]  # .set_index('trading_day')

    c2 = get_sell_date(stock, lag, investor_appetite)
    sell_day = daily_price.loc[[c2[i][0] for i in range(0, len(c2))]][
        ['trading_day', 'CLOSE']]  # .set_index('trading_day')

    plt.figure(figsize=(10, 10))
    plt.scatter(no_action_day['trading_day'], no_action_day['CLOSE'], c='grey', s=10)
    b = plt.scatter(buy_day['trading_day'], buy_day['CLOSE'], c='green', s=80, marker='*', label='buy')
    s = plt.scatter(sell_day['trading_day'], sell_day['CLOSE'], c='red', s=80, marker='*', label='sell')

    plt.show()

    # return
    # plt.show(figsize=(20, 10))#sell_day.plot(style='k.',)


visualize_price(stock='HVN', lag=5, investor_appetite=0.05)