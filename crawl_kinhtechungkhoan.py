import scrapy
import bs4
from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup
import re
import pandas as pd
import requests
from bs4 import BeautifulSoup
from datetime import date
import pymysql

#--------------
def connect():
    conn = pymysql.connect(host='192.168.41.19',
                           port=13306,
                           user='sentiment',
                           password='123456',
                           db='sentiment',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    return conn

industry_index=range(29,37) #index of list of industry 19 bank,
stock_list=[]
for j in industry_index:
    with connect().cursor() as cursor:
        query = "SELECT name FROM sentiment.mxh_industry_with_user;"
        con = connect().cursor()
        con.execute(query)
        results = con.fetchall()
#--------------
    with connect().cursor() as cursor:
        query = "SELECT stock FROM sentiment.mxh_stocks where industry='"+results[j]['name']+"';"# and market='HSX';"
        con = connect().cursor()
        con.execute(query)
        results2 = con.fetchall() 
        stock_list=stock_list+[a['stock'] for a in results2]
        print(results[j]['name'])
        print(len(results2))
print('len total stock_list:',len(stock_list))
stock_list


def get_news_ktck(stock):
    url = "https://kinhtechungkhoan.vn/search_enginer.html?p=tim-kiem&q=" + stock
    soup = BeautifulSoup(requests.get(url).text)
    html = urlopen(url)
    bsObj = BeautifulSoup(html.read())
    href_link = []
    for j in list(bsObj.find_all("li", {"class": "clearfix"})):
        href_link.append(j.a.attrs["href"])
    url_link = []
    date = []
    title = []
    description = []
    content = []
    image = []
    for link in href_link:
        try:
            html_article = urlopen('https://kinhtechungkhoan.vn/' + link)
        except ValueError:
            pass

        bsObj = BeautifulSoup(html_article.read())
        try:
            title.append(bsObj.find('h1', {'class': 'detail-title'}).getText().strip())
            a1 = bsObj.find('span', {'class': 'format_time'}).getText().strip()
            a2 = bsObj.find('span', {'class': 'format_date'}).getText().strip()
            if a2[6:].isdigit() and a2[3:5].isdigit() and a2[:2].isdigit() and a1[3:].isdigit():
                date.append(a2[6:] + '-' + a2[3:5] + '-' + a2[:2] + ' ' + a1)
            else:
                date.append('')
            description.append(bsObj.find('h2', {'class': 'detail-sapo'}).getText().strip())
            content1 = ''
            for j in bsObj.find_all("div", {"class": "__MB_MASTERCMS_EL item-content"})[0].find_all("p", {
                "style": "text-align: justify;"}):
                content1 = content1 + j.getText().strip()
            content.append(content1)
            url_link.append('https://kinhtechungkhoan.vn/' + link)
        except AttributeError:
            pass

        try:
            image.append(bsObj.find_all("div", {"class": "__MB_MASTERCMS_EL item-content"})[0].table.img['src'])
        except AttributeError:
            image.append('')
        # except IndexError:
        #    image.append('')
    df = pd.DataFrame()
    df['url_link'] = url_link
    df['title'] = title
    df['publishdate'] = date
    df['description'] = description
    df['content'] = content
    df['image'] = image

    df['website_name'] = ['https://kinhtechungkhoan.vn'] * len(df)
    df['stock_new'] = [stock] * len(df)
    # df['downloaded_at']=[str(date.today())]*len(df)
    # df['status']=[0]*len(df)

    return df

today=str(date.today())
#----------
crawl_df=get_news_ktck(stock_list[0])#pd.DataFrame()#columns=['url_link', 'title', 'published_date_on_article', 'description','content', 'website_name', 'stock_new', 'downloaded_at', 'status'])
for stock in stock_list:
    crawl_df=crawl_df.append(get_news_ktck(stock),ignore_index=True)
crawl_df['downloaded_at']=[today]*len(crawl_df)
crawl_df['status']=[0]*len(crawl_df)
print('number of article:',len(crawl_df))
print('number of stock:',len(set(list(crawl_df['stock_new']))))

def connect():
    conn = pymysql.connect(host='192.168.41.19',
                           port=13306,
                           user='sentiment',
                           password='123456',
                           db='sentiment',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    return conn
with connect().cursor() as cursor:
    query = 'SELECT * FROM sentiment.mxh_news_by_industry'
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()
last_id=results[len(results)-1]['id']
#results[0].keys()
with connect().cursor() as cursor:
    query = "SELECT original_links FROM mxh_news_by_industry where website_name = 'https://kinhtechungkhoan.vn'"
    con = connect().cursor()
    con.execute(query)
    results = con.fetchall()
i1=[]
for i in range(0,len(crawl_df)):
    if crawl_df.iloc[i]['url_link'] in [a['original_links'] for a in results]:
        i1.append(i)
    else:
        pass
crawl_df=crawl_df.drop(i1)#.drop(columns=['index'])
print('last_id in DB:',last_id)
print(len(crawl_df))

#id,original_links,titles,publishdate_at,discription,content_text_only,website_name, stock_new,downloaded_at,status
query2=tuple([tuple([last_id+i+1]+list(crawl_df.iloc[i])) for i in range(0,len(crawl_df))])
print(query2)
