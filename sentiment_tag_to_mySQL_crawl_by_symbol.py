import mysql.connector
import pandas as pd
from ipynb.fs.defs.unicode_converter import compound_unicode
import datetime

cnx = mysql.connector.connect(
host="192.168.41.19",
port=13306,
user="sentiment",
password="123456")
    # Get a cursor
cursor = cnx.cursor()

cursor.execute("show columns from sentiment.mxh_news_by_industry")
#cursor.execute("select * from sentiment.raw_analysis_report")
db_columns = cursor.fetchall()
db_columns = [a[0] for a in db_columns]

from vncorenlp import VnCoreNLP

# To perform word segmentation, POS tagging, NER and then dependency parsing
#annotator = VnCoreNLP("http://192.168.41.19:8894/edit/engine/VnCoreNLP-1.1.1.jar", annotators="wseg,pos,ner,parse", max_heap_size='-Xmx2g')
#annotator = VnCoreNLP("http://192.168.41.19:8896/tree/engine/VnCoreNLP-1.1.1.jar", annotators="wseg,pos,ner,parse", max_heap_size='-Xmx2g')
annotator = VnCoreNLP("VnCoreNLP-1.1.1.jar", annotators="wseg", max_heap_size='-Xmx500m')

cnx = mysql.connector.connect(
host="192.168.41.19",
port=13306,
user="sentiment",
password="123456")
    # Get a cursor
cursor = cnx.cursor()
data=[]
cursor.execute("select * from sentiment.mxh_news_by_industry")
#cursor.execute("select distinct sentiment_tag from sentiment.mxh_news_by_industry")
for row in cursor:
    data.append(row)
modified_db=pd.DataFrame(data,columns=db_columns)
print(len(modified_db))

modified_db=modified_db[modified_db.website_name==list(set(modified_db['website_name']))[0]]
modified_db=modified_db.reset_index()

not_null_list=[]
for i in range(0,len(modified_db)):
    if modified_db.iloc[i]['sentiment_tag'] in ['POS','NEG','NEU']:
        not_null_list.append(i)
    else:
        pass
modified_db=modified_db.drop(not_null_list)

tokenized_summary1=[]
for text in list(modified_db['discription']):
     tokenized_summary1.append(annotator.tokenize(text))
tokenized_summary=[]
for text in tokenized_summary1:
    try:
        b=text[0]
        for j in range(0,len(text)):
            b=b+text[j]
        tokenized_summary.append(b)
    except IndexError:
        tokenized_summary.append('')

tokenized_content1=[]
for text in modified_db['content_text_only']:
     tokenized_content1.append(annotator.tokenize(text))
tokenized_content=[]
for text in tokenized_content1:
    try:
        b=text[0]
        for j in range(0,len(text)):
            b=b+text[j]
        tokenized_content.append(b)
    except IndexError:
        tokenized_content.append('')

tokenize_db=pd.DataFrame()
tokenize_db['id']=list(modified_db['id'])
tokenize_db['stock']=list(modified_db['stock'])
tokenize_db['tokenized_summary']=tokenized_summary
tokenize_db['tokenized_content']=tokenized_content

cnx = mysql.connector.connect(
host="192.168.41.19",
port=13306,
user="sentiment",
password="123456")
cursor = cnx.cursor()
list_of_keyword=[]
cursor.execute("select * from sentiment._sentiment_termdict")
for row in cursor:
    list_of_keyword.append(row)
term_dict=pd.DataFrame(data=list_of_keyword,columns =['key_word','label'])
tokenize =[a.replace(' ','_') for a in term_dict['key_word']]
term_dict['tokenize']=tokenize

unicode_token_term = [compound_unicode(term) for term in list(term_dict['tokenize'])]
term_dict['unicode_token_term']=unicode_token_term

unicode_tokenized_content=[]
for text in tokenized_content:
    unicode_tokenized_content.append([compound_unicode(a) for a in text])

match_term=[]
for text in unicode_tokenized_content:
    match_term.append([t for t in unicode_token_term if t in text])

#-------
n=[]
sentiment_count=[]
score = []
news_label=[]
for i in range(0,len(match_term)):
    n.append([term_dict.iloc[term_dict[term_dict['unicode_token_term']==match_term[i][j]].index.values[0]]['label'] for j in range(0,len(match_term[i]))])
    sentiment_count.append([n[i].count('positive'),n[i].count('negative')])
    score.append(sentiment_count[i][0]-sentiment_count[i][1])
    if score[i] >= 1:
        news_label.append('POS')
    if score[i] <= -1:
        news_label.append('NEG')
    if score[i] > -1 and score[i] <1:
        news_label.append('NEU')
print(news_label.count('POS'),news_label.count('NEG'),news_label.count('NEU'))

tokenize_db['score']=score
tokenize_db['news_label']=news_label

for i in range(0,len(tokenize_db)):
     print("Update sentiment.mxh_news_by_industry set sentiment_tag='"+tokenize_db.iloc[i]['news_label']+"' where id="+ str(modified_db.iloc[i]['id'])+";")