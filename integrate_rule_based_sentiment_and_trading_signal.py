import pandas as pd
from datetime import datetime
import datetime as dt
from ipynb.fs.defs.MySQL_term_dict import _term_dict
from ipynb.fs.defs.version_label_by_price import get_percentage_change,get_abnormal_return,get_trading_signal_date,visualize_price
from ipynb.fs.defs.elastic2csv_1506 import elasticsearch_stockTicket
from ipynb.fs.defs.MySQL_term_dict import _term_dict


def sentiment_news(stock):
    stock1 = stock.lower()
    output = elasticsearch_stockTicket(stock1)  # pd.DataFrame.from_dict(#,orient='columns')
    output = output.drop(columns=['author']).dropna().reset_index()  # [output.keyword =='cổ phiếu '+stock]
    output = output[['keyword', 'date', 'datetime_object', 'tokenize_content']]
    # -------
    term_dict = _term_dict()
    term_dict = term_dict.drop_duplicates(subset='term').reset_index()

    tokenize_term = []
    tokenize = []
    for i in range(0, len(term_dict)):
        tokenize_term.append([term_dict.iloc[i]['term'].replace(' ', '_'), term_dict.iloc[i]['label']])
        tokenize.append(term_dict.iloc[i]['term'].replace(' ', '_'))

    term_dict['tokenize'] = tokenize

    ##-------------
    ##datetime_str=[output.iloc[i]['date'].split(',')[1][1:] + output.iloc[i]['date'].split(',')[2][:6] for i in range(0,len(output))]
    ##datetime_object = []
    ##for i in range(0,len(output)):
    ##    datetime_object.append(datetime.strptime(datetime_str[i], '%d/%m/%Y %H:%M'))
    ##output['datetime_object'] = datetime_object
    ##output=output.rename(columns={'date':'datetime_object'})
    output = output.sort_values(by=['date']).reset_index()  # .drop(columns=['date'])
    # -------
    trading_day = []
    for i in range(0, len(output)):
        # news after 15pm is attached to next day
        if output.iloc[i]['datetime_object'].dayofweek in (0, 1, 2, 3):
            if output.iloc[i]['datetime_object'].hour < 15:
                trading_day.append(output.iloc[i]['datetime_object'].date())
            else:
                trading_day.append(output.iloc[i]['datetime_object'].date() + pd.Timedelta('1 days'))
        # news on Friday after 15pm and weekend is attached to next Monday
        else:
            if output.iloc[i]['datetime_object'].dayofweek == 4 and output.iloc[i]['datetime_object'].hour < 15:
                trading_day.append(output.iloc[i]['datetime_object'].date())
            if output.iloc[i]['datetime_object'].dayofweek == 4 and output.iloc[i]['datetime_object'].hour >= 15:
                trading_day.append(output.iloc[i]['datetime_object'].date() + pd.Timedelta('3 days'))
            if output.iloc[i]['datetime_object'].dayofweek == 5:
                trading_day.append(output.iloc[i]['datetime_object'].date() + pd.Timedelta('2 days'))
            if output.iloc[i]['datetime_object'].dayofweek == 6:
                trading_day.append(output.iloc[i]['datetime_object'].date() + pd.Timedelta('1 days'))
    output['trading_day'] = trading_day
    # set([trading_day[i].weekday() for i in range(0,len(trading_day))])

    # -------
    match_term = []
    for j in range(0, len(output)):
        match_term.append(list(set(term_dict['tokenize']).intersection(output.iloc[j]['tokenize_content'].split())))
    # -------
    n = []
    sentiment_count = []
    score = []
    news_label = []
    for i in range(0, len(match_term)):
        n.append([term_dict.iloc[term_dict[term_dict['tokenize'] == match_term[i][j]].index.values[0]]['label'] for j in
                  range(0, len(match_term[i]))])
        sentiment_count.append([n[i].count('positive'), n[i].count('negative')])
        score.append(sentiment_count[i][0] - sentiment_count[i][1])
        if score[i] >= 1:
            news_label.append('POSITIVE NEWS')
        if score[i] <= -1:
            news_label.append('NEGATIVE NEWS')
        if score[i] > -1 and score[i] < 1:
            news_label.append('NEUTRAL NEWS')
    output['score'] = score
    output['news_label'] = news_label
    output = output.drop(columns=['index', 'date'])
    ##output = output[output.datetime_object>timeframe[0]][output.datetime_object<timeframe[1]]
    return output

# sentiment_news('MWG')

def match_news_and_trading_signal(stock, lag, investor_appetite):
    # timeframe = [pd.Timestamp('2018-12-31 00:00:00'),pd.Timestamp('2020-01-01 00:00:00')]

    stock1 = stock.lower()
    a = sentiment_news(stock).reset_index()
    # a=a[a.datetime_object>timeframe[0]][a.datetime_object<timeframe[1]].reset_index()
    # date=[]
    # for i in range(0,len(a)):
    #    date.append(a.iloc[i]['datetime_object'].date())
    # a['date']=date
    a1 = a[['trading_day', 'news_label']]
    a1 = a1.groupby(['trading_day']).apply(lambda group_series: group_series.news_label.tolist()).reset_index()
    a1 = a1.rename(columns={0: "sentiment"})
    # ----------

    b = get_trading_signal_date(stock, lag, investor_appetite)
    b = pd.DataFrame(b, columns=['trading_day_timestamp', 'recommended_action'])
    trading_day = [b.iloc[i]['trading_day_timestamp'].date() for i in range(0, len(b))]
    b['trading_day'] = trading_day
    b = b.drop(columns=['trading_day_timestamp'])
    d = pd.merge(a1, b, on='trading_day', how='outer').sort_values('trading_day').fillna('0').reset_index()
    # -------------
    e = d[d.recommended_action != '0'][d.sentiment != '0']
    accurate_sentiment = []
    for i in range(0, len(e)):
        if e.iloc[i]['recommended_action'] == 'BUY':
            if 'POSITIVE NEWS' in e.iloc[i]['sentiment']:

                accurate_sentiment.append('Positive true')
            else:
                if 'NEGATIVE NEWS' in e.iloc[i]['sentiment'] or 'NEUTRAL NEWS' in e.iloc[i]['sentiment']:
                    accurate_sentiment.append('Negative false')

        if e.iloc[i]['recommended_action'] == 'SELL':
            if 'NEGATIVE NEWS' in e.iloc[i]['sentiment']:
                accurate_sentiment.append('Negative true')
            else:
                if 'POSITIVE NEWS' in e.iloc[i]['sentiment'] or 'NEUTRAL NEWS' in e.iloc[i]['sentiment']:
                    accurate_sentiment.append('Positive false')

    e['accurate_sentiment'] = accurate_sentiment
    return e


#match_news_and_trading_signal('MWG', 30, 0.08)

f=match_news_and_trading_signal('MWG',30,0.08)
accurate_sentiment = f['accurate_sentiment']
for result in list(set(accurate_sentiment)):
    print(result,': ',len(f[f.accurate_sentiment==result]))

